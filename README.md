<!--# New repo

This repository has been moved from git.mirkodi.eu since it's closing down. This is its new official home.-->

# Swatch .beat clock for Android

<a href="https://translate.codeberg.org/engage/swatch-beat-clock/">
<img src="https://translate.codeberg.org/widget/swatch-beat-clock/svg-badge.svg" alt="Translation status" />
</a>

Are you tired of the boring old 24-hour format? Could you use some
change in your life?

Let me introduce you to the [Swatch Internet
Time](https://en.wikipedia.org/wiki/Swatch_Internet_Time) format!

Instead of dividing the day into 24 hours, it divides it into 1000
.beat\'s. 6:00 AM becomes \"@250\", 11:27 PM becomes \"@977\" and so on!
In addition to that, there are no timezones in Internet Time. It\'s the
same all over the world. It\'s based on Biel Mean Time (BMT), similar to
[Central European Time
(CET)](https://en.wikipedia.org/wiki/Central_European_Time).

Of course, getting used to this new format might not be easy, and even
when you learn it, you will still need a .beat clock.

That\'s where the Swatch .beat clock app comes into place. With a handy
converter and a fully functional digital clock, it will help you through
your journey.

# Download

[<img src="pix/F-Droid-button.png" width="200px">](https://f-droid.org/en/packages/eu.mirkodi.swatchbeatclock)

# Hacking

If you feel like doing Internet Time yourself, in this repo you can find
a handy InternetTime Java class that you can use to make your own stuff.

# Contributors

- [pesco](https://codeberg.org/pesco)

# License

> Copyright (C) 2023-2024 mirk0dex
>
> This program is free software; you can redistribute it and/or modify
> it under the terms of the GNU General Public License as published by
> the Free Software Foundation; either version 3 of the License, or (at
> your option) any later version.
>
> This program is distributed in the hope that it will be useful, but
> WITHOUT ANY WARRANTY; without even the implied warranty of
> MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
> General Public License for more details.
>
> You should have received a copy of the GNU General Public License
> along with this program; if not, see <https://www.gnu.org/licenses>.
