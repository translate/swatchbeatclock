Non sei stanco del noioso vecchio formato 24-ore? Apprezzeresti un po' di cambiamento nella tua vita?

Permettimi di presentarti il formato <b>Internet Time Swatch</b>!

Anzichè dividere la giornata in 24 ore, la divide in 1000 .beat (o ".battiti"). Le 6:00 diventano "@250", le 23:27 diventano "@966" e così via! Oltre a questo, non ci sono fusi orari nell'Internet Time. È uguale in tutto il mondo. È basato sul Biel Mean Time (BMT), simile al Tempo dell'Europa Centrale (TEC).

Ovviamente, abiturarsi a questo nuovo formato potrebbe non essere facile, e anche quando lo impari, avrai bisogno di un orologio .beat.

È qui che entra in gioco l'app Orologio .beat Swatch. Con un comodo convertitore e un orologio digitale interamente funzionante, ti aiuterà attraverso la tua avventura.

Scopri di più sull'Internet Time su <a href="https://it.wikipedia.org/wiki/Swatch_Internet_Time">Wikipedia</a>.
