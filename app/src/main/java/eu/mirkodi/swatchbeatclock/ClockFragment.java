package eu.mirkodi.swatchbeatclock;

import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import java.text.DecimalFormat;

public class ClockFragment extends Fragment {
    // settings
    private static final int UPDATEINTERVAL = 854 /* ms */; // one .beat is ~85400 ms
    private static final String DECIMALFORMAT_FORMATTER = "#.##";

    private static final DecimalFormat df = (DecimalFormat) new DecimalFormat(DECIMALFORMAT_FORMATTER);

    TextView clock;
    ProgressBar pBar;

    final int PBARMAX = 100;

    private final Handler handler = new Handler(); // "thread"

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.clock, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        View gottenView = getView();

        if (gottenView != null) {
            clock = gottenView.findViewById(R.id.clock);
            pBar = gottenView.findViewById(R.id.pBar);

            pBar.setMax(PBARMAX);

            // define the code block to be executed
            Runnable[] runnables = new Runnable[1];
            runnables[0] = () -> {
                double curTime = InternetTime.getCurrentTimeBeats();
                clock.setText(gottenView.getContext().getString(
                        R.string.beatsDisplay,
                        df.format(curTime)
                ));

                setProgressBar(curTime);

                // repeat every UPDATEINTERVAL ms
                handler.postDelayed(runnables[0], UPDATEINTERVAL);
            };

            // start the Runnable immediately
            handler.post(runnables[0]);
        }
    }

    protected void setProgressBar(double value) {
        // isolate the number after floating point
        double afterFP = value - ((int) value);
        pBar.setProgress((int) (afterFP * PBARMAX) /* 1:value=PBARMAX:x*/);
    }
}
