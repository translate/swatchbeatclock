package eu.mirkodi.swatchbeatclock;

import android.text.InputFilter;
import android.text.Spanned;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.Objects;

// by [Pratik Sharma](https://stackoverflow.com/users/556975/pratik-sharma)

public class InputFilterMinMax implements InputFilter {
    private final double min, max;
    private static final DecimalFormat df = (DecimalFormat) DecimalFormat.getInstance();

    public InputFilterMinMax(double min, double max) {
        this.min = min;
        this.max = max;
    }

    @Override
    public CharSequence filter(CharSequence source, int start, int end, Spanned dest,
                               int dstart, int dend) {
        try {
            double input = Objects.requireNonNull(df.parse(dest.toString() + source.toString())).doubleValue();
            if (isInRange(min, max, input))
                return null;
        } catch (NumberFormatException | ParseException ignored) {
        }
        return "";
    }

    private boolean isInRange(double a, double b, double c) {
        return b > a ? c >= a && c <= b : c >= b && c <= a;
    }
}
