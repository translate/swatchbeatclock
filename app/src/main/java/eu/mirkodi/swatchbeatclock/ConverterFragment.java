package eu.mirkodi.swatchbeatclock;

import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TimePicker;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.TimeZone;

public class ConverterFragment extends Fragment {
    private static final int UNINITIALISED_DIFFERENCE_BETWEEN_SYSTEMTZ_AND_INTERNETTIMETZ = -25;
    private static final String LOGTITLE = "Converter";

    private static final DecimalFormat df = (DecimalFormat) DecimalFormat.getInstance();

    static TimeZone systemTZ;
    static int differenceBetweenSystemTZAndInternetTimeTZ = UNINITIALISED_DIFFERENCE_BETWEEN_SYSTEMTZ_AND_INTERNETTIMETZ;

    TimePicker timePicker;
    EditText converted;
    RadioGroup convertSideChooser;
    Button clear;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.converter, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        View gottenView = getView();

        if (gottenView != null) {
            timePicker = gottenView.findViewById(R.id.normal_date);

            converted = gottenView.findViewById(R.id.beats);

            convertSideChooser = gottenView.findViewById(R.id.convert_side_picker);

            clear = gottenView.findViewById(R.id.clear);

            clear.setOnClickListener(view1 -> {
                timePicker.setHour(0); // run these first
                timePicker.setMinute(0);
                converted.setText(""); // and then this one
            });

            convertSideChooser.setOnCheckedChangeListener((radioGroup, i) -> {
                if (convertSideChooser.getCheckedRadioButtonId() == R.id.normal_chosen) {
                    converted.setEnabled(false);
                    timePicker.setEnabled(true);
                    converted.setFilters(new InputFilter[]{});
                } else {
                    converted.setEnabled(true);
                    timePicker.setEnabled(false);
                    converted.setFilters(new InputFilter[]{new InputFilterMinMax(0, 1000)});
                }
            });

            timePicker.setOnTimeChangedListener((timePicker, i, i1) -> {
                if (convertSideChooser.getCheckedRadioButtonId() == R.id.normal_chosen) {
                    int convertedHour = convertToGMTPlus1(timePicker.getHour());
                    //Log.d("DEBUG", "Converted hour: " + convertedHour);
                    converted.setText(df.format(InternetTime.timeToBeat(convertedHour, timePicker.getMinute(), 0 /* seconds */, 0 /* millis */)));
                }
            });

            converted.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void afterTextChanged(Editable editable) {
                    if (convertSideChooser.getCheckedRadioButtonId() == R.id.beats_chosen) {
                        int[] normalTime;
                        String text = converted.getText().toString();

                        try {
                            normalTime = InternetTime.beatToTime(text);
                        } catch (ParseException e) {
                            Log.e(LOGTITLE, "Got ParseException on " + text);
                            throw new RuntimeException(e);
                        }


                        if (normalTime != null) {
                            // display the result
                            timePicker.setHour(sanitiseHour(convertToSystemTZ(normalTime[0]))
                                    /* convert normalTime[0] (hour) to InternetTime.TIMEZONE */);

                            //Log.d("DEBUG", "Converted written beats hour: "+convertToSystemTZ(normalTime[0]));
                            //Log.d("DEBUG", "Converted, sanitised written beats hour: "+sanitiseHour(convertToSystemTZ(normalTime[0])));
                            //Log.d("DEBUG", "Timepicker hour: "+timePicker.getHour());

                            if (normalTime.length > 1) timePicker.setMinute(normalTime[1]);
                        } else {
                            Log.w(LOGTITLE, "normalTime is null");
                        }
                    }
                }
            });
        }
    }

    public static int sanitiseHour(int hour) {
        return (int) TimeUtils.sanitise(hour, 0, 24);
    }

    private int convertToGMTPlus1(int hour) {
        //Log.d("DEBUG", "convertToGMTPlus1 - hour: " + hour);
        return hour - getDifference(false);
    }

    private int convertToSystemTZ(int hour) {
        //Log.d("DEBUG", "convertToSystemTZ - hour: " + hour);
        return hour + getDifference(false);
    }

    private static TimeZone getSystemTZ(boolean update) {
        if (systemTZ == null || update) {
            systemTZ = TimeZone.getDefault();
        }
        return systemTZ;
    }

    private int getDifference(boolean update) {
        // if differenceBetweenSystemTZAndInternetTimeTZ is uninitialised, or an update was requested
        if (differenceBetweenSystemTZAndInternetTimeTZ == UNINITIALISED_DIFFERENCE_BETWEEN_SYSTEMTZ_AND_INTERNETTIMETZ || update) {
            // initialise it:
            saveDifference(true);
        }
        return differenceBetweenSystemTZAndInternetTimeTZ;
    }

    // initialise or update differenceBetweenSystemTZAndInternetTimeTZ
    private static void saveDifference(boolean updateTZToo) {
        // 1. get current hour in system timezone
        int hourSystemTZ = TimeUtils.getCurrentHour(getSystemTZ(updateTZToo));
        // 2. get current hour in GTM+1
        int hourInternetTimeTZ = TimeUtils.getCurrentHour(InternetTime.TIMEZONE);
        // 3. calculate the difference
        differenceBetweenSystemTZAndInternetTimeTZ = hourSystemTZ - hourInternetTimeTZ;
        //Log.d("DEBUG", "Difference: " + differenceBetweenSystemTZAndInternetTimeTZ);
    }

    // we should make sure that we're using the correct, updated system TimeZone
    public static void timeZoneChanged() {
        saveDifference(true);
    }

    @Override
    public void onResume() {
        saveDifference(true);
        super.onResume();
    }
}
